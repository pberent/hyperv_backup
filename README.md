# Hyper-V Backup

## Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Usage](#usage)
- [TO DO](#todo)

## About <a name = "about"></a>

This is simple PSH script that allow very basic backup of Hyper-V Virtual Machines to external network share.

## Getting Started <a name = "getting_started"></a>

Script will catch ONLY running machines from Hyper-V host, makes snapshot of them, then copy VHDX files to external storage. Then it removes snapshots. 
It can be easily automated with task scheduler.

### Prerequisites

Powershell 5.1 and above <br />
Hyper-V powershell module

### Installing

The best way (for future automation) is to locate script on C:\scripts location

## Usage <a name = "usage"></a>

For running it diretcly from powershell cosnole, to backup and keep last 7 copies:
C:\scripts\corebackup.ps1 -Keep 7 -TargetDir \\\sever01\share -L C:\temp -Subject 'HYPERV BACKUP' -Smtp IP.OF.SMTP.SERVER -SendTo your@mail.com -From HYPERVHOST@yourdomain.com

To run without fancy banner (for task scheduler) use -Nobanner switch

To add it to task scheduler add it with action to start powershell.exe with following arguments:
-ExecutionPolicy Bypass c:\scripts\corebackup.ps1 -Nobanner -Keep 7 -TargetDir \\\sever01\share -L C:\temp -Subject 'HYPERV BACKUP' -Smtp IP.OF.SMTP.SERVER -SendTo your@mail.com -From HYPERVHOST@yourdomain.com

## To do <a name = "todo"></a>

Current version have cluster support, but it is commented in the code intentionally - it goes messed up when balancing rules are in place.